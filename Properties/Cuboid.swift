//
//  Cuboid.swift
//  Properties
//
//  Created by Audhy Virabri Kressa on 29/04/20.
//  Copyright © 2020 Audhy Virabri Kressa. All rights reserved.
//

import Foundation

struct Cuboid {
    var width = 0.0, height = 0.0, depth = 0.0
    var volume: Double {
        return width * height * depth
    }
}
