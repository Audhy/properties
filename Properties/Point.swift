//
//  Point.swift
//  Properties
//
//  Created by Audhy Virabri Kressa on 29/04/20.
//  Copyright © 2020 Audhy Virabri Kressa. All rights reserved.
//

import Foundation

struct Point {
    var x = 0.0, y = 0.0
}
