//
//  Size.swift
//  Properties
//
//  Created by Audhy Virabri Kressa on 29/04/20.
//  Copyright © 2020 Audhy Virabri Kressa. All rights reserved.
//

import Foundation

struct Size {
    var width = 0.0, height = 0.0
}
